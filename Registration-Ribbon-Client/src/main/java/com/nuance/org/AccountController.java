package com.nuance.org;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;

@Controller
public class AccountController {
	
	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	private Client client;
	
	@RequestMapping("/")
	public String home() throws RestClientException, IOException{
	//public @ResponseBody String  showLoginPage(ModelMap model) throws RestClientException, IOException{
		//Client client = new Client();
		//System.out.println(client.getEmployee("/"));
		return "index";
	}
	
	@RequestMapping("/accountList")
	public String accountList(Model model) throws Throwable, IOException {
		model.addAttribute("accounts", accountRepository.getAllAccounts());
		//Client client = new Client();
	
	//	System.out.println(client.getEmployee("/accountList"));
		return "accountList";
	}
	
	@RequestMapping("/accountDetails")
	public String accountDetails(@RequestParam("number") String id, Model model) throws Throwable, IOException {
		model.addAttribute("account", accountRepository.getAccount(id));
	//	Client client = new Client();
	//	System.out.println(client.getEmployee("/accountDetails"));
		return "accountDetails";
	}
	@RequestMapping("/backend")
	public String accountDetails( Model model) throws Throwable, IOException {
		model.addAttribute("account", accountRepository.getBackend()  );
	//	Client client = new Client();
	//	System.out.println(client.getEmployee("/accountDetails"));
		return "backend";
	}
}

