package com.nuance.org;

import java.util.List;

public interface AccountRepository {
	
	List<Account> getAllAccounts();
	
	Account getAccount(String number);
	
	Account[] getBackend();
}
