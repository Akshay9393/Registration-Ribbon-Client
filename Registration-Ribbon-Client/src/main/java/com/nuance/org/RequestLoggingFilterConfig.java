package com.nuance.org;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.boot.actuate.trace.http.HttpTrace.Response;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import com.netflix.client.http.HttpRequest;

@Configuration
public class RequestLoggingFilterConfig {
  
   @Bean
      public FilterRegistrationBean logFilter() {
          CommonsRequestLoggingFilter filter
            = new CommonsRequestLoggingFilter();
          FilterRegistrationBean registration = new FilterRegistrationBean();
          registration.setName("requestLoggingFilter");
          registration.setDispatcherTypes(DispatcherType.REQUEST);
          registration.setFilter(new CommonsRequestLoggingFilter());
          registration.addUrlPatterns("/**");
          registration.setOrder(Integer.MAX_VALUE);
          return registration;
          
		/*
		 * filter.setIncludeClientInfo(true); filter.getFilterConfig();
		 * //filter.doFilter(HttpRequest request, HttpResponse response, FilterChain
		 * filterChain); filter.setIncludeQueryString(true);
		 * filter.setIncludePayload(true); filter.setMaxPayloadLength(10000);
		 * filter.setIncludeHeaders(false);
		 * filter.setAfterMessagePrefix("REQUEST DATA : "); return filter;
		 */
      }
}
